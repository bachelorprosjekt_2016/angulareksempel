<!DOCTYPE html>
<!--
Copyright(C) 2016.  All rights reserved to Bjørnholt school. 
https://bjornholt.osloskolen.no/
@author Created by Bachelor Final Project group 18 at Oslo and Akershus University College 
Creating interactive web pages using the Angualr framework carried out by:
Martin Hansen Muren Mathisen (s189116), Waqas Liaqat (s180364), 
Yuanxin Huang (s184519), Thanh Nguyen Chu (s169954)
@version 1.01.01
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>AngularJS friendsPlan App</title>
        <!-- Bootstrap css-->
        <link rel="stylesheet" 
              href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" 
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="style.css" 
              type="text/css"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <!-- Bootstrap Js -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->  
        <meta name="viewport" content="width=device-width. initial-scale=1">
    </head>
    <body>
        
        <nav class="nav"> 
            <div class="container-fluid">
                 <a id="logo_main" class="pull-left col-sm-3" href="#/"><img src="logo.svg" width="30px" height="30px" alt="logo View">
                </a>
                <div ng-class="language" class="pull-right navbar-header">
                    <a type="button" class="btn btn-default">English</a>
                    <a type="button" class="btn btn-default">Norsk</a>
                </div>
                <center>
                <div ng-class="title" class="col-md-6 col-md-offset-3">
                    <h1>DATA REGISTER - BJH</h1>
                </div>
            </div>
        </nav>
        
        <div class = "row main-content">
            <div class="col-md-4 col-md-offset-4 login-wrapper lightblue-bg box-shadow">
                <center>
                    <div class="padding-top-20">
                        <img src="images/user.png" 
                             class="img-circle box-shadow" 
                             style= "height:60px;width:60px"/>
                    </div>
                <h1>Admin Logg inn</h1>
                    
                </center>
                <div id= "form-wrapper">
                    <form id="login-form" action="index.php" role ="form">    
                        <div class="form-group">
                            <label for="admin_norwegian_id">Fødselsnummer:</label>
                            <input type="text" class="form-control" 
                                   placeholder="Fødselsnummer" 
                                   autofocus 
                                   id = "admin_norwegian_id">
                        </div>
                        <div class="form-group">
                            <label for="password">Passord:</label>
                            <input type="password" 
                                   class="form-control" 
                                   placeholder="Passord" 
                                   id="password">
                        </div>
                        <a href="#">Glemt passord?</a>
                        <button class="btn btn-default pull-right" type="submit">Logg inn</button>
                        
                    </form>
                    </center>
                </div>
            </div>
        </div>



            <script src="js/angular.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular-resource.min.js"></script>
            <script src="js/angular-route.min.js"></script>
            <script type='text/javascript' src='controller.js'></script>

    </body>
</html>


