<!DOCTYPE html>
<!--
Copyright(C) 2016.  All rights reserved to Bjørnholt school. 
https://bjornholt.osloskolen.no/
@author Created by Bachelor Final Project group 18 at Oslo and Akershus University College 
Creating interactive web pages using the Angualr framework carried out by:
Martin Hansen Muren Mathisen (s189116), Waqas Liaqat (s180364), 
Yuanxin Huang (s184519), Thanh Nguyen Chu (s169954)
@version 1.01.01
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>AngularJS friendsPlan App</title>
        <!-- Bootstrap css-->
        <link rel="stylesheet" 
              href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" 
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="style.css" 
              type="text/css"/>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <!-- Bootstrap Js -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->  
        <meta name="viewport" content="width=device-width. initial-scale=1">
    </head>
    <body>
<!--        <div align="left">
            <img src="">
        </div>
        
        
        <div ng-class="language" align="right">
            <button type="button" class="btn btn-default">English</button>
            <button type="button" class="btn btn-default">Norsk</button>
        </div>


        <div ng-class="title" align="center">
            <h1>DATA REGISTER - BJH</h1>
        </div>

        <br/>
        
        <div ng-class="row">
            <div id="inner">
            <div class="col-md-4 col-xs-10 col-xs-offset-1 col-md-offset-4 bg-success lightblue-bg box-shadow">
                <br/>
               
                <center>
                    <img src="images/mypc_lock.png" width="70" height="70" class="img-circle bg-primary">
                </center>
                <div ng-app="myApp" ng-controller="customersCtrl">

                    <center><h1>Bruker Logg inn</h1></center>

                   <form role = "form">
                        <div class = "form-group">
                            <label for="norwegian_id"> Fødselsnummer (11 siffer):</label>

                            <input type="text" class="form-control" id="norwegian_id" ng-model="norwegian_id">
                        </div>

                        <div class="form-group">
                            <label for="password">Passord: </label>
                            <input type="password" class="form-control" id="password" ng-model="password">
                        </div>

                        <a href="#">Glemt passord?</a>
                        <button type="submit" class="btn btn-default pull-right" ng-click="login()">Logg inn</button>
                        
                    </form>


                    

                  
                    
                     tester ut noe 
                    
                    <?php
//                    
//                    echo "<form name='login_form' ng-controller='loginController' ng-submit='login(credentials)' novalidate>";
//                    echo "<label for='norwegian_id'>Fødselsnummer (11 siffer):</label>";
//                    echo "<br/>";
//                    echo "<input type='text' id='norwegian_id' ng-model='credentials.norwegian_id'>";
//                    echo "<br/>";
//                    echo "<label for='password'>Passord:</label>";
//                    echo "<br/>";
//                    echo "<input type='password' id='password' ng-model='credentials.password'>";
//                    echo "<br/>";
//                    echo "<a href=''>Glemt passord?</a>";
//                    echo "<button type='submit' class='pull-right'>Login</button>";
//                    echo "</form>";
//                    
//                    
//                    $norwegian_id_in = "";
//                    $password_in = "";
//                    ?>
                    
                    <ul>
                        <li ng-repeat="x in myData">
                            {{x.NorwegianID + ', ' + x.Password}}
                        </li>
                    </ul>
                </div>
                </div>
            </div>
        </div>-->

<nav class="nav"> 
            <div class="container-fluid">
                 <a id="logo_main" class="pull-left col-sm-3" href="#/"><img src="logo.svg" width="30px" height="30px" alt="logo View">
                </a>
                <div ng-class="language" class="pull-right navbar-header">
                    <a type="button" class="btn btn-default">English</a>
                    <a type="button" class="btn btn-default">Norsk</a>
                </div>
                <center>
                <div ng-class="title" class="col-md-6 col-md-offset-3">
                    <h1>DATA REGISTER - BJH</h1>
                </div>
            </div>
        </nav>
        
        <div class = "row main-content">
            <div class="col-md-4 col-md-offset-4 login-wrapper lightblue-bg box-shadow">
                <center>
                    <div class="padding-top-20">
                        <img src="images/admin.png" 
                             class="img-circle box-shadow" 
                             style= "height:60px;width:60px"/>
                    </div>
               
                <h1>Bruker Logg inn</h1>
                    
                </center>
                <div id= "form-wrapper">
                    <form id="login-form" action="index.php" role ="form">    
                        <div class="form-group">
                            <label for="admin_norwegian_id">Fødselsnummer:</label>
                            <input type="text" class="form-control" 
                                   placeholder="Fødselsnummer" 
                                   autofocus 
                                   id = "admin_norwegian_id">
                        </div>
                        <div class="form-group">
                            <label for="password">Passord:</label>
                            <input type="password" 
                                   class="form-control" 
                                   placeholder="Passord" 
                                   id="password">
                        </div>
                        <a href="#">Glemt passord?</a>
                        <button class="btn btn-default pull-right" type="submit">Logg inn</button>
                        
                    </form>
                    </center>
                </div>
            </div>
        </div>

        <script src="js/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular-resource.min.js"></script>
        <script src="js/angular-route.min.js"></script>
        <script type='text/javascript' src='controller.js'></script>

    </body>
</html>